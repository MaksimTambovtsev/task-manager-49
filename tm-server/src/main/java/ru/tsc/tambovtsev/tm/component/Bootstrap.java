package ru.tsc.tambovtsev.tm.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.apache.activemq.broker.BrokerService;
import org.apache.log4j.BasicConfigurator;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.tambovtsev.tm.api.endpoint.*;
import ru.tsc.tambovtsev.tm.api.repository.IDomainRepository;
import ru.tsc.tambovtsev.tm.api.repository.dto.IProjectRepository;
import ru.tsc.tambovtsev.tm.api.repository.dto.ITaskRepository;
import ru.tsc.tambovtsev.tm.api.repository.dto.IUserRepository;
import ru.tsc.tambovtsev.tm.api.service.*;
import ru.tsc.tambovtsev.tm.api.service.dto.*;
import ru.tsc.tambovtsev.tm.endpoint.*;
import ru.tsc.tambovtsev.tm.enumerated.Role;
import ru.tsc.tambovtsev.tm.dto.model.UserDTO;
import ru.tsc.tambovtsev.tm.repository.DomainRepository;
import ru.tsc.tambovtsev.tm.repository.dto.ProjectRepository;
import ru.tsc.tambovtsev.tm.repository.dto.TaskRepository;
import ru.tsc.tambovtsev.tm.repository.dto.UserRepository;
import ru.tsc.tambovtsev.tm.service.*;
import ru.tsc.tambovtsev.tm.service.dto.*;
import ru.tsc.tambovtsev.tm.util.HashUtil;
import ru.tsc.tambovtsev.tm.util.SystemUtil;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.xml.ws.Endpoint;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

public final class Bootstrap implements IServiceLocator {

    @Getter
    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IDatabaseProperty databaseProperties = propertyService;

    @Getter
    @NotNull
    private final IConnectionService connectionService = new ConnectionService(databaseProperties);

    @NotNull
    private final IDomainRepository domainRepository = new DomainRepository(this);

    @Getter
    @NotNull
    private final EntityManager connection = connectionService.getEntityManager();

    @Getter
    @NotNull
    private final IProjectRepository projectRepositoryDTO = new ProjectRepository(connection);

    @Getter
    @NotNull
    private final ITaskRepository taskRepositoryDTO = new TaskRepository(connection);

    @Getter
    @NotNull
    private final IUserRepository userRepositoryDTO = new UserRepository(connection);

    @Getter
    @NotNull
    private final ITaskService taskServiceDTO = new TaskService(taskRepositoryDTO, connectionService);

    @Getter
    @NotNull
    private final IProjectService projectServiceDTO = new ProjectService(projectRepositoryDTO, connectionService);

    @Getter
    @NotNull
    private final IProjectTaskService projectTaskServiceDTO = new ProjectTaskService(
            projectRepositoryDTO, taskRepositoryDTO, connectionService
    );

    @Getter
    @NotNull
    private final ILoggerService loggerService = new LoggerService();

    @Getter
    @NotNull
    private final IUserService userServiceDTO = new UserService(
            connectionService, userRepositoryDTO,  propertyService, projectRepositoryDTO, taskRepositoryDTO
    );

    @Getter
    @NotNull
    private final IAuthService authService = new AuthService(propertyService, userServiceDTO);

    @Getter
    @NotNull
    private final IDomainService domainService = new DomainService(domainRepository);

    @NotNull
    private final IDomainEndpoint domainEndpoint = new DomainEndpoint(this);

    @NotNull
    private final IProjectEndpoint projectEndpoint = new ProjectEndpoint(this);

    @NotNull
    private final IUserEndpoint userEndpoint = new UserEndpoint(this);

    @NotNull
    private final IAuthEndpoint authEndpoint = new AuthEndpoint(this);

    @NotNull
    private final ITaskEndpoint taskEndpoint = new TaskEndpoint(this);

    private final ISystemEndpoint systemEndpoint = new SystemEndpoint(this);

    @NotNull
    private final Backup backup = new Backup(this);

    @NotNull
    private final CalculatorEndpoint calculatorEndpoint = new CalculatorEndpoint();

    {
        registry(calculatorEndpoint);
        registry(systemEndpoint);
        registry(projectEndpoint);
        registry(domainEndpoint);
        registry(taskEndpoint);
        registry(userEndpoint);
        registry(authEndpoint);
    }

    public void run() {
        createTable();
        initPID();
        initUsers();
        loggerService.info("** TASK-MANAGER SERVER STARTED **");
        Runtime.getRuntime().addShutdownHook(new Thread(this::prepareShutdown));
        initJMS();
    }

    private void prepareShutdown() {
        loggerService.info("** TASK-MANAGER SERVER STOPPED **");
        backup.stop();
    }

    @SneakyThrows
    private void initUsers() {
        @Nullable UserDTO user = userServiceDTO.findByLogin("test");
        if (user == null) {
            user = new UserDTO();
            user.setEmail("test@test.ru");
            user.setLogin("test");
            user.setPasswordHash(HashUtil.salt(propertyService, "test"));
            userServiceDTO.create(user);
        }
        user = userServiceDTO.findByLogin("admin");
        if (user == null) {
            user = new UserDTO();
            user.setRole(Role.ADMIN);
            user.setLogin("admin");
            user.setPasswordHash(HashUtil.salt(propertyService, "admin"));
            userServiceDTO.create(user);
        }
    }

    private void registry(@NotNull final Object endpoint) {
        @NotNull final String host = "localhost";
        @NotNull final String port = "8080";
        @NotNull final String name = endpoint.getClass().getSimpleName();
        @NotNull final String url = "http://" + host + ":" + port + "/" + name + "?WSDL";
        Endpoint.publish(url, endpoint);
        System.out.println(url);
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

    @SneakyThrows
    private void initJMS() {
        BasicConfigurator.configure();
        @NotNull final BrokerService broker = new BrokerService();
        broker.addConnector("tcp://localhost:61616");
        broker.start();
    }

    @SneakyThrows
    private void createTable() {
        final IConnectionService connectionService = new ConnectionService(databaseProperties);
        @NotNull final EntityManagerFactory entityManagerFactory = connectionService.getHibernateFactory();
        @NotNull final EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.close();
        entityManagerFactory.close();
    }

}