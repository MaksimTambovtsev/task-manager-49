package ru.tsc.tambovtsev.tm.repository.model;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.tambovtsev.tm.api.repository.model.IRepository;
import ru.tsc.tambovtsev.tm.model.AbstractEntity;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.ArrayList;
import java.util.List;

public abstract class AbstractRepository<M extends AbstractEntity> implements IRepository<M> {

    @NotNull
    protected static final String USER_ID = "USER_ID";

    @NotNull
    protected static final String ID = "ID";

    @NotNull
    protected static final String PROJECT_ID = "PROJECT_ID";

    @NotNull
    protected static final String LOGIN = "LOGIN";

    @NotNull
    protected static final String EMAIL = "EMAIL";

    @NotNull
    protected static final String NAME = "NAME";

    @NotNull
    protected static final String DESCRIPTION = "DESCRIPTION";

    @NotNull
    protected static final String STATUS = "STATUS";

    @NotNull
    protected static final String PASSWORD_HASH = "PASSWORD_HASH";

    @NotNull
    protected static final String ROLE = "ROLE";

    @NotNull
    protected static final String FIRSTNAME = "FIRSTNAME";

    @NotNull
    protected static final String LASTNAME = "LASTNAME";

    @NotNull
    protected static final String MIDDLENAME = "MIDDLENAME";

    @NotNull
    protected static final String LOCKED = "LOCKED";

    @NotNull
    protected final EntityManager entityManager;

    @NotNull
    protected final List<M> models = new ArrayList<>();

    protected AbstractRepository(@NotNull EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @NotNull
    protected abstract String getTableName();

    @Nullable
    protected <M> M getResult(TypedQuery<M> query) {
        final List<M> result = query.getResultList();
        if (result.isEmpty()) return null;
        return (M) result.get(0);
    }

    @Nullable
    @Override
    public abstract M findById(@Nullable final String id);

    @Override
    @SneakyThrows
    public void removeById(@Nullable final String id) {
        entityManager
                .createQuery("DELETE FROM " + getTableName() + " WHERE ID = :id")
                .setParameter("id", id)
                .executeUpdate();
    }

    @Override
    @SneakyThrows
    public void removeCascade(@NotNull M model) {
        entityManager
                .remove(model);
    }

    @Override
    public void create(@NotNull M model) {
        entityManager.persist(model);
    }

    @Override
    public void update(@NotNull M model) {
        entityManager.merge(model);
    }

}
