package ru.tsc.tambovtsev.tm.service.model;


import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.tambovtsev.tm.api.repository.model.IProjectRepository;
import ru.tsc.tambovtsev.tm.api.service.IConnectionService;
import ru.tsc.tambovtsev.tm.api.service.model.IProjectService;
import ru.tsc.tambovtsev.tm.enumerated.Sort;
import ru.tsc.tambovtsev.tm.enumerated.Status;
import ru.tsc.tambovtsev.tm.exception.AbstractException;
import ru.tsc.tambovtsev.tm.exception.field.*;
import ru.tsc.tambovtsev.tm.model.Project;
import ru.tsc.tambovtsev.tm.repository.model.ProjectRepository;

import javax.persistence.EntityManager;

import java.util.*;

public class ProjectService extends AbstractUserOwnedService<Project, IProjectRepository> implements IProjectService {

    public ProjectService(
            @NotNull final IProjectRepository repository,
            @NotNull IConnectionService connection
    ) {
        super(repository, connection);
    }

    @NotNull
    @Override
    public IProjectRepository getRepository(@NotNull EntityManager entityManager) {
        return new ProjectRepository(entityManager);
    }

    @Nullable
    @Override
    @SneakyThrows
    public Project changeProjectStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) throws AbstractException {
        Optional.ofNullable(userId).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(id).orElseThrow(IdEmptyException::new);
        @NotNull final EntityManager entityManager = connection.getEntityManager();
        @NotNull final IProjectRepository repository = new ProjectRepository(entityManager);
        try {
            final Project project = repository.findById(userId, id);
            if (project == null) return null;
            entityManager.getTransaction().begin();
            project.setStatus(status);
            repository.updateById(project);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e){
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return null;
    }

    @Override
    @SneakyThrows
    public void removeById(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        Optional.ofNullable(userId).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(id).orElseThrow(IdEmptyException::new);
        @NotNull final EntityManager entityManager = connection.getEntityManager();
        @NotNull final IProjectRepository repository = new ProjectRepository(entityManager);
        try {
            @Nullable final Project result = repository.findById(userId, id);
            if (result == null) return;
            entityManager.getTransaction().begin();
            repository.removeByIdProject(userId, id);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e){
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public List<Project> findAll(
            @Nullable final String userIdPr,
            @Nullable final Sort sort
    ) {
        Optional.ofNullable(userIdPr).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(sort).orElseThrow(NameEmptyException::new);
        @NotNull final EntityManager entityManager = connection.getEntityManager();
        @NotNull final IProjectRepository repository = new ProjectRepository(entityManager);
        try {
            return repository.findAll(userIdPr, sort);
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public List<Project> findAll() {
        @NotNull final EntityManager entityManager = connection.getEntityManager();
        @NotNull final IProjectRepository repository = new ProjectRepository(entityManager);
        try {
            return repository.findAllProject();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void addAll(@NotNull final Collection<Project> models) {
        @NotNull final EntityManager entityManager = connection.getEntityManager();
        try {
            @NotNull final IProjectRepository repository = new ProjectRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.addAll(models);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void clearProject() {
        @NotNull final EntityManager entityManager = connection.getEntityManager();
        try {
            @NotNull final IProjectRepository repository = new ProjectRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.clearProject();
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public Collection<Project> set(@NotNull final Collection<Project> models) {
        @NotNull final EntityManager entityManager = connection.getEntityManager();
        try {
            @NotNull final IProjectRepository repository = new ProjectRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.clearProject();
            repository.addAll(models);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return models;
    }

}
