package ru.tsc.tambovtsev.tm.api.service.dto;

import ru.tsc.tambovtsev.tm.api.repository.dto.IOwnerRepository;
import ru.tsc.tambovtsev.tm.dto.model.AbstractUserOwnedModelDTO;

public interface IUserOwnedService<M extends AbstractUserOwnedModelDTO> extends IOwnerRepository<M>, IService<M> {
}
