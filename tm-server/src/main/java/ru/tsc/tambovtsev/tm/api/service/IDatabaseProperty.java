package ru.tsc.tambovtsev.tm.api.service;

import org.jetbrains.annotations.NotNull;

public interface IDatabaseProperty {

    @NotNull String getDatabaseUsername();

    @NotNull String getDatabasePassword();

    @NotNull String getDatabaseUrl();

    @NotNull String getDatabaseDriver();

    @NotNull String getSQLDialect();

    @NotNull String getHbm2ddlAuto();

    @NotNull String getShowSql();

    @NotNull String getFormatSql();

    @NotNull String getSecondLvlCash();

    @NotNull String getFactoryClass();

    @NotNull String getUseQueryCash();

    @NotNull String getUseMinPuts();

    @NotNull String getRegionPrefix();

    @NotNull String getConfigFilePath();

}
