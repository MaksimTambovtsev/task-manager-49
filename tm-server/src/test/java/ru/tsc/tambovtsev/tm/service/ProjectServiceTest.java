package ru.tsc.tambovtsev.tm.service;


import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import ru.tsc.tambovtsev.tm.api.repository.dto.IProjectRepository;
import ru.tsc.tambovtsev.tm.api.repository.dto.ITaskRepository;
import ru.tsc.tambovtsev.tm.api.repository.dto.IUserRepository;
import ru.tsc.tambovtsev.tm.api.service.IConnectionService;
import ru.tsc.tambovtsev.tm.api.service.IPropertyService;
import ru.tsc.tambovtsev.tm.api.service.dto.IProjectService;
import ru.tsc.tambovtsev.tm.api.service.dto.IProjectTaskService;
import ru.tsc.tambovtsev.tm.api.service.dto.ITaskService;
import ru.tsc.tambovtsev.tm.api.service.dto.IUserService;
import ru.tsc.tambovtsev.tm.dto.model.ProjectDTO;
import ru.tsc.tambovtsev.tm.dto.model.TaskDTO;
import ru.tsc.tambovtsev.tm.dto.model.UserDTO;
import ru.tsc.tambovtsev.tm.enumerated.Status;
import ru.tsc.tambovtsev.tm.repository.dto.ProjectRepository;
import ru.tsc.tambovtsev.tm.repository.dto.TaskRepository;
import ru.tsc.tambovtsev.tm.repository.dto.UserRepository;
import ru.tsc.tambovtsev.tm.service.dto.ProjectService;
import ru.tsc.tambovtsev.tm.service.dto.ProjectTaskService;
import ru.tsc.tambovtsev.tm.service.dto.TaskService;
import ru.tsc.tambovtsev.tm.service.dto.UserService;
import ru.tsc.tambovtsev.tm.util.HashUtil;

import java.util.List;

public final class ProjectServiceTest {

    private static IPropertyService propertyService;

    private static IConnectionService connectionService;

    @Nullable
    private IProjectService projectService;

    @Nullable
    private ITaskService taskService;

    @Nullable
    private IProjectTaskService projectTaskService;

    @NotNull
    private IUserService userService;

    @NotNull
    private IProjectRepository projectRepository;

    @NotNull
    private ITaskRepository taskRepository;

    @NotNull
    private IUserRepository userRepository;

    @Nullable
    private UserDTO user = new UserDTO();

    @Nullable
    private ProjectDTO project;

    @Nullable
    private TaskDTO task;

    @Nullable
    private String projectId;

    @Nullable
    private String taskId;

    @BeforeClass
    public static void initConnection() {
        propertyService = new PropertyService();
        connectionService = new ConnectionService(propertyService);
    }

    @AfterClass
    public static void disconnectConnection() {
        connectionService.close();
    }

    @Before
    public void setProjectService() {
        projectRepository = new ProjectRepository(connectionService.getEntityManager());
        taskRepository = new TaskRepository(connectionService.getEntityManager());
        userRepository = new UserRepository(connectionService.getEntityManager());
        userService = new UserService(
                connectionService, userRepository,  propertyService, projectRepository, taskRepository
        );
        projectService = new ProjectService(projectRepository, connectionService);
        taskService = new TaskService(taskRepository, connectionService);
        projectTaskService = new ProjectTaskService(projectRepository, taskRepository, connectionService);
        @Nullable ProjectDTO project = new ProjectDTO();
        @Nullable TaskDTO task = new TaskDTO();
        user.setEmail("test@test.ru");
        user.setLogin("test");
        user.setPasswordHash(HashUtil.salt(propertyService, "test"));
        userService.create(user);
        project.setUserId(user.getId());
        project.setName("123");
        project.setDescription("432");
        projectService.create(project);
        project = new ProjectDTO();
        project.setUserId(user.getId());
        project.setName("1321");
        project.setDescription("234");
        projectService.create(project);
        projectId = project.getId();
        task.setUserId(user.getId());
        task.setName("1111");
        task.setDescription("22222");
        taskService.create(task);
        taskId = task.getId();
    }

    @After
    public void clearProjectService() {
        taskService.clearTask();
        projectService.clearProject();
        userService.removeByLogin("test");
    }

    @Test
    public void testFindAll() {
        Assert.assertFalse(projectService.findAll().isEmpty());
    }

    @Test
    public void testFindAllNegative() {
        Assert.assertFalse(projectService.findAll().get(0).getName().isEmpty());
    }

    @Test
    public void testFindById() {
        @Nullable final List<ProjectDTO> projects = projectService.findAll();
        @Nullable final String projectId = projects.stream().findFirst().get().getId();
        Assert.assertFalse(projectService.findById(projectId).getName().isEmpty());
    }

    @Test
    public void testRemove() {
        project = projectService.findAll().get(1);
        projectService.removeById(project.getId());
        Assert.assertNotEquals(project.getId(), projectService.findAll().get(0).getId());
    }

    @Test
    public void testRemoveById() {
        project = projectService.findAll().get(1);
        projectService.removeById(project.getId());
        Assert.assertNotEquals(project.getId(), projectService.findAll().get(0).getId());
    }

    @Test
    public void testUpdate() {
        project = projectService.findAll().get(0);
        project.setName("Project1");
        project.setDescription("Descriptions");
        projectService.update(project);
        Assert.assertEquals(projectService.findById(project.getId()).getName(), "Project1");
    }

    @Test
    public void changeProjectStatusById() {
        project = projectService.findAll().get(0);
        projectService.changeProjectStatusById(project.getUserId(), project.getId(), Status.COMPLETED);
        Assert.assertEquals(projectService.findById(project.getId()).getStatus(), Status.COMPLETED);
    }

    @Test
    public void testClear() {
        projectService.clear(user.getId());
        Assert.assertTrue(projectService.findAll().isEmpty());
    }

    @Test
    public void testCascadeRemove() {
        project = projectService.findById(projectId);
        task = taskService.findById(taskId);
        Assert.assertNotNull(task);
        task.setProjectId(projectId);
        taskService.update(task);
        projectTaskService.removeProjectById(user.getId(), projectId);
        Assert.assertNull(taskService.findById(taskId));
    }

}
