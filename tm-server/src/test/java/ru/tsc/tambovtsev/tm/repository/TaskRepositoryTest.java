package ru.tsc.tambovtsev.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import ru.tsc.tambovtsev.tm.api.repository.dto.IProjectRepository;
import ru.tsc.tambovtsev.tm.api.repository.dto.ITaskRepository;
import ru.tsc.tambovtsev.tm.api.repository.dto.IUserRepository;
import ru.tsc.tambovtsev.tm.api.service.IConnectionService;
import ru.tsc.tambovtsev.tm.api.service.IPropertyService;
import ru.tsc.tambovtsev.tm.api.service.dto.ITaskService;
import ru.tsc.tambovtsev.tm.api.service.dto.IUserService;
import ru.tsc.tambovtsev.tm.dto.model.TaskDTO;
import ru.tsc.tambovtsev.tm.dto.model.UserDTO;
import ru.tsc.tambovtsev.tm.enumerated.Sort;
import ru.tsc.tambovtsev.tm.repository.dto.TaskRepository;
import ru.tsc.tambovtsev.tm.repository.dto.UserRepository;
import ru.tsc.tambovtsev.tm.service.ConnectionService;
import ru.tsc.tambovtsev.tm.service.PropertyService;
import ru.tsc.tambovtsev.tm.service.dto.TaskService;
import ru.tsc.tambovtsev.tm.service.dto.UserService;
import ru.tsc.tambovtsev.tm.util.HashUtil;

import javax.persistence.EntityManager;
import java.util.List;

public final class TaskRepositoryTest {

    private static IPropertyService propertyService;

    private static IConnectionService connectionService;

    private static EntityManager entityManager;

    @NotNull
    private IProjectRepository projectRepository;

    @NotNull
    private ITaskRepository taskRepository;

    @NotNull
    private IUserRepository userRepository;

    @Nullable
    private ITaskService taskService;

    @NotNull
    private IUserService userService;

    @Nullable
    private UserDTO user = new UserDTO();

    @BeforeClass
    public static void initConnection() {
        propertyService = new PropertyService();
        connectionService = new ConnectionService(propertyService);
        entityManager = connectionService.getEntityManager();
    }

    @AfterClass
    public static void disconnectConnection() {
        entityManager.close();
        connectionService.close();
    }

    @Before
    public void setTaskRepository() {
        taskRepository = new TaskRepository(entityManager);
        userRepository = new UserRepository(entityManager);
        taskService = new TaskService(taskRepository, connectionService);
        userService = new UserService(
                connectionService, userRepository,  propertyService, projectRepository, taskRepository
        );
        @Nullable TaskDTO task = new TaskDTO();
        user.setEmail("test@test.ru");
        user.setLogin("test");
        user.setPasswordHash(HashUtil.salt(propertyService, "test"));
        userService.create(user);
        task.setUserId(user.getId());
        task.setName("123");
        task.setDescription("432");
        taskService.create(task);
        task = new TaskDTO();
        task.setUserId(user.getId());
        task.setName("1321");
        task.setDescription("234");
        taskService.create(task);
    }

    @After
    public void clearTaskRepository() {
        taskService.clearTask();
    }

    @Test
    public void testFindAll() {
        Assert.assertFalse(taskRepository.findAll(user.getId(), Sort.NAME).isEmpty());
    }

    @Test
    public void testFindAllNegative() {
        Assert.assertFalse(taskRepository.findAll(user.getId(), Sort.NAME).isEmpty());
    }

    @Test
    public void testFindById() {
        @Nullable final List<TaskDTO> tasks = taskRepository.findAll(user.getId(), Sort.NAME);
        @Nullable final String taskId = tasks.stream().findFirst().get().getId();
        Assert.assertFalse(taskRepository.findById(taskId).getName().isEmpty());
    }

    @Test
    public void testUpdateById() {
        @NotNull final TaskDTO task = taskRepository.findAll(user.getId(), Sort.NAME).get(1);
        task.setName("333-333-333-333");
        entityManager.getTransaction().begin();
        taskRepository.updateById(task);
        entityManager.getTransaction().commit();
        Assert.assertEquals(taskRepository.findById(task.getId()).getName(), "333-333-333-333");
    }

    @Test
    public void testRemoveById() {
        @NotNull final String taskId = taskRepository.findAll(user.getId(), Sort.NAME).get(1).getId();
        entityManager.getTransaction().begin();
        taskRepository.removeById(user.getId(), taskId);
        entityManager.getTransaction().commit();

        Assert.assertNull(taskRepository.findById(user.getId(), taskId));
    }

    @Test
    public void testClear() {
        entityManager.getTransaction().begin();
        taskRepository.clearTask();
        entityManager.getTransaction().commit();
        Assert.assertTrue(taskRepository.findAll(user.getId(), Sort.NAME).isEmpty());
    }

}
