package ru.tsc.tambovtsev.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import ru.tsc.tambovtsev.tm.api.repository.dto.IProjectRepository;
import ru.tsc.tambovtsev.tm.api.repository.dto.ITaskRepository;
import ru.tsc.tambovtsev.tm.api.repository.dto.IUserRepository;
import ru.tsc.tambovtsev.tm.api.service.IConnectionService;
import ru.tsc.tambovtsev.tm.api.service.IPropertyService;
import ru.tsc.tambovtsev.tm.api.service.dto.ITaskService;
import ru.tsc.tambovtsev.tm.api.service.dto.IUserService;
import ru.tsc.tambovtsev.tm.dto.model.UserDTO;
import ru.tsc.tambovtsev.tm.repository.dto.UserRepository;
import ru.tsc.tambovtsev.tm.service.ConnectionService;
import ru.tsc.tambovtsev.tm.service.PropertyService;
import ru.tsc.tambovtsev.tm.service.dto.UserService;

import javax.persistence.EntityManager;
import java.util.Arrays;

public final class UserRepositoryTest {

    private static IPropertyService propertyService;

    private static IConnectionService connectionService;

    private static EntityManager entityManager;

    @NotNull
    private IProjectRepository projectRepository;

    @NotNull
    private ITaskRepository taskRepository;

    @NotNull
    private IUserRepository userRepository;

    @Nullable
    private ITaskService taskService;

    @NotNull
    private IUserService userService;

    @Nullable
    private UserDTO user = new UserDTO();

    @BeforeClass
    public static void initConnection() {
        propertyService = new PropertyService();
        connectionService = new ConnectionService(propertyService);
        entityManager = connectionService.getEntityManager();
    }

    @AfterClass
    public static void disconnectConnection() {
        entityManager.close();
        connectionService.close();
    }

    @Before
    public void setUserRepository() {
        userRepository = new UserRepository(entityManager);
        userService = new UserService(
                connectionService, userRepository,  propertyService, projectRepository, taskRepository
        );
        @NotNull UserDTO[] usersArray = {
                new UserDTO(
                "testUnit1",
                        "242342342342",
                        "qwerty@test.ru",
                        "First",
                        "Last"),
                new UserDTO(
                        "testUnit2",
                        "345342342342",
                        "qwerty2@test.ru",
                        "First2",
                        "Last2")
            };

        Arrays.stream(usersArray).forEach(userService::create);
    }

    @After
    public void clearUserRepository() {
        entityManager.getTransaction().begin();
        userRepository.clearUser();
        entityManager.getTransaction().commit();
    }

    @Test
    public void testFindAll() {
        Assert.assertFalse(userRepository.findAllUser().isEmpty());
    }

    @Test
    public void testFindAllNegative() {
        Assert.assertFalse(userRepository.findAllUser().get(0).getId().isEmpty());
    }

    @Test
    public void testFindByLogin() {
        @Nullable final UserDTO findUser =  userRepository.findByLogin("testUnit1");
        Assert.assertFalse(findUser.getLogin().isEmpty());
    }

    @Test
    public void testFindByEmail() {
        @Nullable final UserDTO findUser =  userRepository.findByEmail("qwerty@test.ru");
        Assert.assertFalse(findUser.getEmail().isEmpty());
    }

    @Test
    public void testRemoveById() {
        @Nullable final UserDTO findUser =  userRepository.findAllUser().get(0);
        entityManager.getTransaction().begin();
        userRepository.removeById(findUser.getId());
        entityManager.getTransaction().commit();
        Assert.assertNotEquals(userRepository.findAllUser().get(0).getId(), findUser.getId());
    }

    @Test
    public void testRemoveByLogin() {
        @Nullable final UserDTO findUser =  userRepository.findAllUser().get(0);
        entityManager.getTransaction().begin();
        userRepository.removeByLogin(findUser.getLogin());
        entityManager.getTransaction().commit();
        Assert.assertNotEquals(userRepository.findAllUser().get(0).getId(), findUser.getId());
    }

    @Test
    public void testClear() {
        entityManager.getTransaction().begin();
        userRepository.clearUser();
        entityManager.getTransaction().commit();
        Assert.assertTrue(userRepository.findAllUser().isEmpty());
    }

}
