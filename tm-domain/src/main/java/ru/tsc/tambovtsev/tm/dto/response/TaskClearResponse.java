package ru.tsc.tambovtsev.tm.dto.response;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public final class TaskClearResponse extends AbstractResponse {

}
