package ru.tsc.tambovtsev.tm.api;

import org.jetbrains.annotations.NotNull;
import ru.tsc.tambovtsev.tm.dto.logger.EntityLogDTO;

public interface ILoggerService {

    void writeLog(@NotNull EntityLogDTO message);

}
