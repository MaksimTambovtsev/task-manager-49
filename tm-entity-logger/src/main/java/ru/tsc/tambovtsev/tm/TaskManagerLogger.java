package ru.tsc.tambovtsev.tm;

import org.jetbrains.annotations.NotNull;
import ru.tsc.tambovtsev.tm.component.Bootstrap;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public final class TaskManagerLogger {

    public static void main(@NotNull final String[] args) {
        @NotNull final Bootstrap bootstrap = new Bootstrap();
        bootstrap.init();
    }

}