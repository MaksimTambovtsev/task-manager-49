package ru.tsc.tambovtsev.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.tambovtsev.tm.api.ILoggerService;
import ru.tsc.tambovtsev.tm.dto.logger.EntityLogDTO;

import java.io.File;
import java.io.FileOutputStream;
import java.nio.file.Paths;

public final class LoggerService implements ILoggerService {

    @NotNull
    private static final String PATH_LOG_FILE = "./log-sql/";

    @NotNull
    private static final String PROJECT_LOG_FILE_NAME = PATH_LOG_FILE + "project.entity.logger.txt";

    @NotNull
    private static final String TASK_LOG_FILE_NAME = PATH_LOG_FILE + "task.entity.logger.txt";

    @NotNull
    private static final String USER_LOG_FILE_NAME = PATH_LOG_FILE + "user.entity.logger.txt";

    @Override
    @SneakyThrows
    public void writeLog(@NotNull final EntityLogDTO message) {
        @Nullable final String className = message.getClassName();
        @Nullable final String fileName = getFileName(className);
        if (fileName == null) return;
        @NotNull final File file = new File(fileName);
        file.getParentFile().mkdirs();
        @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(file, true);
        @NotNull final String header =
                "Id: " + message.getId() + "; Type: " + message.getType() + "; Date: " + message.getDate() + "\n";
        fileOutputStream.write(header.getBytes());
        fileOutputStream.write(message.getEntity().getBytes());
        fileOutputStream.write("\n".getBytes());
        fileOutputStream.flush();
        fileOutputStream.close();
    }

    private String getFileName(@NotNull final String className) {
        switch (className) {
            case "ProjectDTO":
            case "Project":
                return PROJECT_LOG_FILE_NAME;
            case "TaskDTO":
            case "Task":
                return TASK_LOG_FILE_NAME;
            case "UserDTO":
            case "User":
                return USER_LOG_FILE_NAME;
            default:
                return null;
        }
    }

}
