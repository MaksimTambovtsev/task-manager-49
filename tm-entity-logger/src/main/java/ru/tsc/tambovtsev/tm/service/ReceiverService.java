package ru.tsc.tambovtsev.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.tambovtsev.tm.api.IReceiverService;

import javax.jms.*;

public class ReceiverService implements IReceiverService {

    @NotNull
    private final ConnectionFactory connectionFactory;

    public ReceiverService(@NotNull ConnectionFactory connectionFactory) {
        this.connectionFactory = connectionFactory;
    }

    @Override
    @SneakyThrows
    public void receive(@NotNull final MessageListener listener) {
        @NotNull final Connection connection = connectionFactory.createConnection();
        connection.start();
        @NotNull final Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        @NotNull final Destination destination = session.createTopic("TM_LOGGER");
        final MessageConsumer consumer = session.createConsumer(destination);
        consumer.setMessageListener(listener);
    }

}
