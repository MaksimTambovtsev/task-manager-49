package ru.tsc.tambovtsev.tm.component;

import lombok.SneakyThrows;
import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.jetbrains.annotations.NotNull;
import ru.tsc.tambovtsev.tm.api.IReceiverService;
import ru.tsc.tambovtsev.tm.listener.LoggerListener;
import ru.tsc.tambovtsev.tm.service.*;

public final class Bootstrap {

    @SneakyThrows
    public void init() {
        @NotNull final ActiveMQConnectionFactory factory =
                new ActiveMQConnectionFactory(ActiveMQConnection.DEFAULT_BROKER_URL);
        factory.setTrustAllPackages(true);
        @NotNull final IReceiverService receiverService = new ReceiverService(factory);
        receiverService.receive(new LoggerListener());
    }

}