package ru.tsc.tambovtsev.tm.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Getter;
import lombok.SneakyThrows;
import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.jetbrains.annotations.NotNull;
import ru.tsc.tambovtsev.tm.api.service.ISenderService;
import ru.tsc.tambovtsev.tm.dto.logger.EntityLogDTO;

import javax.jms.*;
import javax.xml.bind.annotation.XmlRootElement;

@Getter
@XmlRootElement
public class SenderService implements ISenderService {

    @NotNull
    private final ConnectionFactory connectionFactory =
            new ActiveMQConnectionFactory(ActiveMQConnection.DEFAULT_BROKER_URL);

    @Override
    @SneakyThrows
    public void send(@NotNull EntityLogDTO entity) {
        @NotNull final Connection connection = connectionFactory.createConnection();
        connection.start();
        @NotNull final Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        @NotNull final Destination destination = session.createTopic("TM_LOGGER");
        final MessageProducer messageProducer = session.createProducer(destination);
        final ObjectMessage objectMessage = session.createObjectMessage(entity);
        messageProducer.send(objectMessage);
        messageProducer.close();
        session.close();
        connection.close();
    }

    @NotNull
    @Override
    @SneakyThrows
    public EntityLogDTO createMessage(@NotNull Object object, @NotNull String type) {
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(object);
        @NotNull final String className = object.getClass().getSimpleName();
        @NotNull final EntityLogDTO message = new EntityLogDTO(className, json, type);
        return message;
    }

}
